# Projets de travail pour QGIS du réseau LPO Auvergne-Rhône-Alpes

Les projets sont les fichiers portant l'extension `qgs` pour qgis2 et `qgz` pour qgis3.

Ces projets intègrent des actions facilitant l'exploitation des données. Elles sont présentées dans le dossier `actions_scripts`:

- `synthese` : Les actions attribuées à la couche zone d'étude produisant des tableaux de synthèses pour une zone d'étude:
  - liste des espèces
  - etat des connaissances par groupe taxonomique
  - graphique du nombre de données par groupe taxonomique
- `extraction_*.py` : Script pour extraire les données d'une zone d'étude

Le dépot d'origine du projet est sur [Framagit](https://framagit.org/lpoaura/qgis2bddlpoaura/)

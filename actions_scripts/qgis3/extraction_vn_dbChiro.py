#!/usr/bin/python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------
# Script d'extraction automatique des données sur une zone d'étude
# Type:         Python
# Description:  Extraction de données depuis une zone d'étude
# Nom court:    Extraction
# ----------------------------------------------------------------


from qgis.gui import QgsMessageBar
from qgis.core import QgsDataSourceUri, QgsMessageLog
from qgis.utils import iface
import psycopg2

import datetime

import processing

# Récupération des paramètres de connection de la couche active (paramètres de connection à la bdd postgresql)
layer = iface.activeLayer()
uri = QgsDataSourceUri(layer.dataProvider().dataSourceUri())

dbservice = uri.service()
if uri.sslMode() == 3:
    dbsslmode = 'require'

conn = psycopg2.connect(service=dbservice, sslmode=dbsslmode)

# Déclaration des variables depuis la couche active dans le dictionnaire 'values'
original_values = {}
original_values['reference'] = '[% reference %]'
original_values['id'] = int('[% id %]')
original_values['date'] = datetime.datetime.now().strftime("%Y%m%d")


# reference = 'zetest_201809'
# id = 6090
# 3 - Execution de l extraction des donnees depuis la zone d'etude
# On part du principe que le nom de connection de la base est identique au nom de la base de donnée


def loadGeoTable(values, uri, queries):
    """Création et chargement d'une table géographique"""

    try:
        cur = conn.cursor()
        for query in queries:
            cur.execute(query)
        cur.close()
        conn.commit()
        iface.messageBar().pushMessage(
            "Success", "Table {tablename} creee".format(**values))
    except (Exception, psycopg2.DatabaseError) as error:
        QgsMessageLog.logMessage(str(error))
        iface.messageBar().pushMessage("Erreur à la création de la table",
                                       str(error), level=Qgis.Critical)

    try:
        uri2 = QgsDataSourceUri()
        uri2.setConnection(uri.service(), uri.database(),
                           uri.username(), uri.password(), uri.sslMode())
        # Appel de la couche nouvellement créée
        uri2.setDataSource(None, values['tablename'], "geom", None, "id")
        # chargement de la couche en mémoire (avec son nom à afficher)
        vlayer = QgsVectorLayer(uri2.uri(),
                                values['layername'],
                                "postgres")
        # Chargement de la couche sur la carte
        root = QgsProject.instance().layerTreeRoot()
        exportgrp = root.findGroup('Export')
        if not exportgrp:
            exportgrp = root.insertGroup(0, 'Export')
        QgsProject.instance().addMapLayers([vlayer], False)
        # node_layer = QgsLayerTreeLayer(vlayer)
        exportgrp.addLayer(vlayer)
        iface.messageBar().pushMessage(
            "Success", "Couche {layername} chargee".format(**values))
        # Zoom sur la couche créée
        iface.setActiveLayer(vlayer)
        iface.zoomToActiveLayer()
    except Exception as error:
        QgsMessageLog.logMessage(str(error))
        iface.messageBar().pushMessage("Erreur au chargement de la couche",
                                       str(error), level=Qgis.Critical)


def extraction(values, uri):
    values = original_values
    values['tablename'] = 'ze_{reference}_datavnchiro'.format(**values)
    values['layername'] = 'Extraction des donnees de la zone d\'etude du {date}'.format(
        **values)

    query = ("""drop table if exists {tablename}""".format(**values), """create table {tablename} as
             select df.*
             from
             zone_etude.zone_etude as ze, data.data_fusion as df
where st_within(df.geom, ze.geom) and ze.id = {id}
    """.format(**values),
             """alter table {tablename} add primary key (id)""".format(
                 **values),
             """create index on {tablename} using GIST(geom)""".format(
                 **values),
             """select populate_geometry_columns()""")

    loadGeoTable(values, uri, query)


extraction(original_values, uri)

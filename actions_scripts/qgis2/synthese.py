#!/usr/bin/python3
# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------------
# Script de synthèse automatique des données à partir d'une zone d'étude
# Type:         Python
# Description:  Synthèse de donnée générique
# Nom court:    Synthese
# -------------------------------------------------------------------------------

from qgis.gui import QgsMessageBar
from qgis.utils import iface
import psycopg2
import matplotlib.pyplot as plt
import numpy as np

import processing

# Récupération des paramètres de connection de la couche active (paramètres de connection à la bdd postgresql)
layer = iface.activeLayer()
uri = QgsDataSourceURI(layer.dataProvider().dataSourceUri())

dbhost = uri.host()
dbport = uri.port()
dbuser = uri.username()
dbpwd = uri.password()
dbname = uri.database()
if uri.sslMode() == 3:
    dbsslmode = 'require'

conn = psycopg2.connect(host=uri.host(), port=dbport, dbname=dbname, user=dbuser, sslmode=dbsslmode, password=dbpwd)

# Déclaration des variables depuis la couche active dans le dictionnaire 'values'
reference = '[% reference %]'
id = [% id %]
#reference = 'testze'
#id = 499
values = {'reference': reference, 'id': id}


def loadNonGeoTable(values, uri, query, layername, tablename):
    """
    Synthèse de données sur le polygone cliqué
       - values: les valeurs "référence" et "id" de la table zone d'étude
    """
    querySynth = query
    try:
        processing.runalg('qgis:postgisexecutesql', uri.database(), querySynth)
        iface.messageBar().pushMessage("Success", "Table {} creee".format(tablename))
    except:
        iface.messageBar().pushMessage("Error", "Désolé, la création de la table de synthèse n'a pas fonctionné",
                                       level=QgsMessageBar.CRITICAL)
    # Chargement de la table d'extraction des donnees
    # Création d'une nouvelle connection à partir des paramètres de connection détectés précédemments
    uri2 = QgsDataSourceURI()
    uri2.setConnection(uri.host(), uri.port(), uri.database(), uri.username(), uri.password(), sslmode=uri.sslMode())
    # Appel de la couche nouvellement créée
    uri2.setDataSource("", tablename, None, "", "id")
    # chargement de la couche en mémoire (avec son nom à afficher)
    try:
        vlayer = QgsVectorLayer(uri2.uri(),
                                layername,
                                "postgres")
        iface.messageBar().pushMessage("Success", "Table {} préchargée".format(tablename))
    except:
        iface.messageBar().pushMessage("Error", "Désolé, la création de la table de synthèse n'a pas fonctionné",
                                       level=QgsMessageBar.CRITICAL)
    # Chargement de la couche sur la carte dans un groupe 'Export' créé si absent
    try:
        root = QgsProject.instance().layerTreeRoot()
        exportgrp = root.findGroup('Export')
        if not exportgrp:
            exportgrp = root.insertGroup(0, 'Export')
        QgsMapLayerRegistry.instance().addMapLayers([vlayer], False)
        exportgrp.addLayer(vlayer)
        # Passage de la couche en 'actif' et affichage de la table d'attributs
        iface.setActiveLayer(vlayer)
        iface.showAttributeTable(vlayer)
        iface.messageBar().pushMessage("Success",
                                       "couche \"{}\" chargée".format(layername))
    except:
        iface.messageBar().pushMessage("Error", "Désolé, le chargement de la couche de synthèse n'a pas fonctionné",
                                       level=QgsMessageBar.CRITICAL)


def graphSynth(values, conn):
    cur = conn.cursor()

    cur.execute("""
        select 
            taxonomy_name, 
            count(*) 
        from 
            data.vn as vn,
            zone_etude.zone_etude as ze
        where 
            st_within(vn.geom, ze.geom) 
            and ze.id = {}
        group by
            taxonomy_name
        order by count(*) asc;
    """.format(values['id']))

    lib = []
    value = []
    for record in cur:
        lib.append(record[0])
        value.append(int(record[1]))

    plt.rcdefaults()
    fig, ax = plt.subplots()

    x_pos = np.arange(len(lib))

    ax.bar(x_pos, value, align='center',
           color='#1779ba', ecolor='#333333')
    ax.set_xticks(x_pos)
    ax.set_xticklabels(lib)
    ax.invert_xaxis()  # labels read top-to-bottom
    ax.set_ylabel(u'Nombre de données')
    ax.set_title(u'Etat des connaissances par groupes d\'espèces')

    plt.show()


def synthlistsp(values, uri):
    tablenamesynthlistesp = 'ze_{}_list_sp'.format(values['reference'])

    layernamesynthlisesp = 'SynthDonnees ze {}'.format(values['reference'])

    values['tablename'] = tablenamesynthlistesp

    querysynthlistesp = """
    						drop table if exists {tablename}
    						;

    						create table {tablename} as (
    							with datavn as (
    									select
    											df.id_vn                            as vn_id
    										, df.nom_sci
    										, df.nom_vern
    										, count(df.id)                        as nb_donnee
    										, count(distinct df.observateur)      as nb_observateur
    										, max(code_nidif)                     as max_atlas_code
    										, max(extract(year from date)) :: int as derniere_obs
    									from data.data_fusion as df left join referentiel.statut_nidif sn
    											on df.code_nidif_oiseau = sn.code_repro
    										,
    										zone_etude.zone_etude as ze
    									where ze.id = {id} and st_within(df.geom, ze.geom)
    									group by df.id_vn, df.nom_sci,
    										df.nom_vern), synth as (
    									select distinct
    										d.vn_id
    										, vntr.taxref_id
    										, vntr.vn_grouptax
    										, case when vntr.rang in ('es', 'sses')
    										then 'Esp. vraie'
    											else null end       as espece
    										, case when vntr.taxref_nom_vern is not null
    										then vntr.taxref_nom_vern
    											else d.nom_vern end as nom_francais
    										, case when vntr.taxref_nom_sci is not null
    										then vntr.taxref_nom_sci
    											else d.nom_sci end  as nom_scientifique
    										, d.nb_donnee
    										, d.nb_observateur
    										, d.derniere_obs
    										, sn2.statut_nidif
    									from datavn d left join referentiel.corresp_vn_taxref vntr
    											on vntr.vn_id = d.vn_id
    										left join referentiel.statut_nidif sn2
    											on d.max_atlas_code = sn2.code_nidif
    									order by vntr.vn_grouptax, d.vn_id)
    							select
    								row_number()
    								over () as id
    								, s.*
    							from synth as s)
    						;

    						alter table {tablename}
    							add primary key (id)
    						;
    				  """.format(**values)

    loadNonGeoTable(values, uri, querysynthlistesp, layernamesynthlisesp, tablenamesynthlistesp)


def etatconn(values, uri):
    tablenameetatconn = 'ze_{}_etat_connaissance'.format(values['reference'])

    layernameetatconn = 'EtatConn ze {}'.format(values['reference'])

    values['tablename'] = tablenameetatconn

    queryetatconn = """
                drop table if exists {tablename}
                ;

                create table {tablename} as (
                    select distinct
                            row_number()
                            over ()       as id
                        , taxonomy_name as Groupe_taxonomique
                        , count(distinct dat.id_vn
                            )
                                filter (where vt.rang in ('es', 'sses'
                                )
                                )
                                                        as Taxons
                        , count(distinct observateur
                            )             as Observateurs
                        , count(dat.id
                            )             as Observations
                    from data.data_fusion dat
                        left join referentiel.corresp_vn_taxref vt
                            on dat.id_vn = vt.vn_id ,
                        zone_etude.zone_etude z
                    where z.id = {id} and st_intersects(dat.geom, z.geom)
                    group by taxonomy_name
                    order by Observations desc)
                ;

                alter table {tablename}
                    add primary key (id)
                ;
                        """.format(**values)

    loadNonGeoTable(values, uri, queryetatconn, layernameetatconn, tablenameetatconn)


synthlistsp(values, uri)
etatconn(values, uri)
graphSynth(values, conn)


# 2018-07-17

* **mise à jour**
* `scripts/synthese.pu` : Script de synthèse par zone d'étude générant une table de synthèse par espèce, une table d'état des connaissances et un graphique du nombre de données par espèces.


# 2017-11-20

* **mise à jour**
* 'action_scripts/*' : Ajout des créations automatiques des clés primaires et des index spatiaux

# 2017-11-15
    
* **initial push**
* `action_scripts/zone_etude.py` : Export auto dans le groupe de couches Export, créé si besoin
* `action_scripts/zone_etude.py` : Ajout de l'action de synthèse et export auto dans le groupe de couches Export, créé si besoin
